package com.example.rakutentest.models.entities.api.callbacks


enum class ApiError {
    UNKNOWN_HOST,
    UNKNOWN,
    MALFORMED_JSON
}

interface DataLoadingCallback {
    fun onError(error: ApiError)
}