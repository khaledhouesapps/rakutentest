package com.example.rakutentest.models.entities.ui

import com.example.rakutentest.models.entities.Repo

fun Repo.toRepoUi(): RepoUi = RepoUi(
    name = name,
    language = language,
    creationDate = creationDate.substring(startIndex = 0, endIndex = creationDate.indexOf("T")),
    updateDate = updateDate.substring(startIndex = 0, endIndex = updateDate.indexOf("T")),
    hasIssues = hasIssues,
    forkPolicy = forkPolicy,
    hasWiki = hasWiki,
    size = size,
    type = type,
    hasWebsite = website.isNotEmpty(),
    website = website,
    ownerName = owner.name,
    ownerType = owner.type,
    ownerAvatar = owner.links.avatar.avatarLink
)