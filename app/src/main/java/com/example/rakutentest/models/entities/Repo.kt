package com.example.rakutentest.models.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Repo(
    val name: String,
    @SerializedName("full_name")
    val fullName: String,
    val language: String,
    @SerializedName("created_on")
    val creationDate: String,
    @SerializedName("updated_on")
    val updateDate: String,
    @SerializedName("has_issues")
    val hasIssues: Boolean,
    @SerializedName("fork_policy")
    val forkPolicy: String,
    @SerializedName("has_wiki")
    val hasWiki: Boolean,
    val size: Long,
    val type: String,
    val website: String,
    val owner: RepoOwner
) : Serializable {
    override fun toString(): String {
        return "Repo(name='$name', fullName='$fullName', language='$language', creationDate='$creationDate', type='$type', website='$website', owner=$owner)"
    }
}

class RepoOwner(
    @SerializedName("display_name")
    val name: String,
    val type: String,
    val links: RepoOwnerLinks

) : Serializable {
    override fun toString(): String {
        return "RepoOwner(name='$name', type='$type', links=$links)"
    }
}

class RepoOwnerLinks(
    val avatar: AvatarLink
) : Serializable {
    override fun toString(): String {
        return "RepoOwnerLinks(avatar=$avatar)"
    }
}

class AvatarLink(
    @SerializedName("href")
    val avatarLink: String
) : Serializable {
    override fun toString(): String {
        return "AvatarLink(avatarLink='$avatarLink')"
    }
}

class RepoListApiResponse(
    @SerializedName("values")
    val items: List<Repo>,
    @SerializedName("pagelen")
    val itemsCount: Int,
    @SerializedName("next")
    val nextPageLink: String
) : Serializable {
    override fun toString(): String {
        return "RepoListApiResponse(items=$items, itemsCount=$itemsCount, nextPageLink='$nextPageLink')"
    }
}