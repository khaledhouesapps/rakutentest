package com.example.rakutentest.models.entities.ui

import java.io.Serializable

class RepoUi(
    val name: String,
    val language: String,
    val creationDate: String,
    val updateDate: String,
    val hasIssues: Boolean,
    val forkPolicy: String,
    val hasWiki: Boolean,
    val hasWebsite: Boolean,
    val size: Long,
    val type: String,
    val website: String,
    val ownerName: String,
    val ownerType: String,
    val ownerAvatar: String
): Serializable