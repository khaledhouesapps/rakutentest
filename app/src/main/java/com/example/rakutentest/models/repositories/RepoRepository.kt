package com.example.rakutentest.models.repositories

import androidx.lifecycle.MutableLiveData
import com.example.rakutentest.models.entities.api.callbacks.DataLoadingCallback
import com.example.rakutentest.models.entities.RepoListApiResponse
import com.example.rakutentest.models.entities.api.callbacks.ApiError
import com.example.rakutentest.models.entities.api.services.IRepositoriesService
import com.example.rakutentest.models.entities.ui.RepoUi
import com.example.rakutentest.models.entities.ui.toRepoUi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

class RepoRepository(val reposService: IRepositoriesService?) {

    val reposLiveData: MutableLiveData<List<RepoUi>> = MutableLiveData()
    private var nextQueryParam: String? = null

    fun fetchRepos(wantNextPage: Boolean = false, callback: DataLoadingCallback) {
        val call: Call<RepoListApiResponse>? = if (wantNextPage && nextQueryParam != null) {
            reposService?.fetchRepositories(nextQueryParam!!)
        } else {
            reposService?.fetchRepositories()
        }
        call?.enqueue(object : Callback<RepoListApiResponse> {
            override fun onFailure(call: Call<RepoListApiResponse>?, t: Throwable?) {
                callback.onError(
                    when (t) {
                        is UnknownHostException -> ApiError.UNKNOWN_HOST
                        else -> ApiError.UNKNOWN
                    }
                )
            }

            override fun onResponse(call: Call<RepoListApiResponse>?, response: Response<RepoListApiResponse>?) {
                try {
                    reposLiveData.postValue(response?.body()?.items?.map {
                        it.toRepoUi()
                    })
                    nextQueryParam = response?.body()?.nextPageLink?.indexOf(string = "after=")
                        ?.let { response.body()?.nextPageLink?.substring(startIndex = it + 6) }
                } catch (ex: Exception) {
                    callback.onError(ApiError.MALFORMED_JSON)
                }
            }
        })

    }
}