package com.example.rakutentest.models.entities.api.services

import com.example.rakutentest.models.entities.RepoListApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface IRepositoriesService {
    @GET("repositories")
    fun fetchRepositories(): Call<RepoListApiResponse>

    @GET("repositories")
    fun fetchRepositories(
        @Query(
            value = "after",
            encoded = true
        ) after: String
    ): Call<RepoListApiResponse>
}