package com.example.rakutentest.models.entities.api.retrofitinstance

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClientInstance {
    private val appRoot: String
        get() {
            val mProtocol = "https"
            val mHost = "api.bitbucket.org/2.0"
            return "$mProtocol://$mHost/"
        }

    private var retrofit: Retrofit? = null
    val retrofitInstance: Retrofit?
        get() {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(appRoot)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
}

