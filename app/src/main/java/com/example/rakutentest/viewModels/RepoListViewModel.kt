package com.example.rakutentest.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.rakutentest.models.entities.api.callbacks.DataLoadingCallback
import com.example.rakutentest.models.entities.api.retrofitinstance.RetrofitClientInstance
import com.example.rakutentest.models.entities.api.services.IRepositoriesService
import com.example.rakutentest.models.entities.ui.RepoUi
import com.example.rakutentest.models.repositories.RepoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class RepoListViewModel(application: Application) : AndroidViewModel(application) {
    private val reposService: IRepositoriesService? =
        RetrofitClientInstance.retrofitInstance?.create(
            IRepositoriesService::class.java
        )
    var repoRepository: RepoRepository = RepoRepository(reposService)

    var reposListLiveData: LiveData<List<RepoUi>>

    init {
        reposListLiveData = repoRepository.reposLiveData
    }

    fun fetchRepositories(callback: DataLoadingCallback) =
        viewModelScope.launch(Dispatchers.IO) {
            repoRepository.fetchRepos(
                !reposListLiveData.value.isNullOrEmpty(),
                callback
            )
        }

}