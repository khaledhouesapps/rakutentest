package com.example.rakutentest.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.rakutentest.models.entities.ui.RepoUi

class RepoDetailsViewModel(application: Application) : AndroidViewModel(application) {
    var repo: RepoUi? = null
}