package com.example.rakutentest.views.repoList

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rakutentest.R
import com.example.rakutentest.databinding.ActivityRepoListBinding
import com.example.rakutentest.models.entities.api.callbacks.ApiError
import com.example.rakutentest.models.entities.api.callbacks.DataLoadingCallback
import com.example.rakutentest.models.entities.ui.RepoUi
import com.example.rakutentest.viewModels.RepoListViewModel
import com.example.rakutentest.views.repoDetails.RepoDetailsActivity
import com.example.rakutentest.views.utils.EndlessRecyclerViewScrollListener

class RepoListActivity : AppCompatActivity() {
    private lateinit var activityBinding: ActivityRepoListBinding
    private lateinit var viewModel: RepoListViewModel
    private lateinit var dataLoadingCallback: DataLoadingCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_list)

        activityBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_repo_list)
        viewModel = ViewModelProvider(this).get(RepoListViewModel::class.java)

        setupReposRecyclerView()
        initViewEvents()

        viewModel.reposListLiveData.observe(this, Observer {
            if (it != null) {
                activityBinding.btnNextRepos.visibility = View.GONE
                (activityBinding.rvRepos.adapter as ReposRecyclerViewAdapter).setItems(it)
            } else {
                Log.e("TAG", "ERROR")
            }
        })

        dataLoadingCallback = object : DataLoadingCallback {
            override fun onError(error: ApiError) {
                Log.e("TAG", "ERROR $error")
            }
        }

        viewModel.fetchRepositories(callback = dataLoadingCallback)

    }

    private fun initViewEvents() {
        (activityBinding.rvRepos.adapter as ReposRecyclerViewAdapter).setClickListener(
            object :
                ReposRecyclerViewAdapter.OnItemClickListener {
                override fun onItemClick(position: Int, item: RepoUi) {
                    val intent =
                        Intent(this@RepoListActivity, RepoDetailsActivity::class.java)
                    intent.putExtra("Repo", item)
                    startActivity(intent)
                }
            })

        activityBinding.btnNextRepos.setOnClickListener {
            viewModel.fetchRepositories(callback = dataLoadingCallback)
        }
    }

    private fun setupReposRecyclerView() {
        val viewManager = LinearLayoutManager(this)
        activityBinding.rvRepos.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = ReposRecyclerViewAdapter()
        }

        activityBinding.rvRepos.addOnScrollListener(object :
            EndlessRecyclerViewScrollListener(viewManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                activityBinding.btnNextRepos.visibility = View.VISIBLE
            }
        })
    }
}