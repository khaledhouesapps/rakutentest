package com.example.rakutentest.views.repoList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil.DiffResult
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.rakutentest.R
import com.example.rakutentest.models.entities.Repo
import com.example.rakutentest.databinding.RvItemRepoBinding
import com.example.rakutentest.models.entities.ui.RepoUi
import java.lang.Exception

class ReposRecyclerViewAdapter :
    RecyclerView.Adapter<ReposRecyclerViewAdapter.ItemViewHolder>() {

    private var repos: ArrayList<RepoUi> = ArrayList()
    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val rvItemBinding: RvItemRepoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.rv_item_repo,
            parent,
            false
        )
        return ItemViewHolder(rvItemBinding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item: RepoUi = repos[position]
        holder.itemRepoBinding.item = item
        try {
            Glide.with(holder.itemRepoBinding.root)
                .load(item.ownerAvatar)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_bitbucket))
                .into(holder.itemRepoBinding.imgRepo)

        } catch (exception: Exception) {
            exception.printStackTrace()
        }

        setAnimation(holder.itemRepoBinding.layoutRepo)

        holder.itemRepoBinding.root
            .setOnClickListener {
                if (listener != null && position != DiffResult.NO_POSITION) {
                    listener!!.onItemClick(position, repos[position])
                }
            }
    }

    private fun setAnimation(viewToAnimate: View) {
        val animation =
            AnimationUtils.loadAnimation(viewToAnimate.context, android.R.anim.slide_in_left)
        viewToAnimate.startAnimation(animation)
    }

    override fun getItemCount(): Int {
        return repos.size
    }

    fun setItems(newItems: List<RepoUi>) {
        repos.addAll(newItems)
        notifyItemRangeInserted(repos.size, newItems.size)
    }

    fun setClickListener(itemClickListener: OnItemClickListener?) {
        listener = itemClickListener
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, item: RepoUi)
    }

    class ItemViewHolder(var itemRepoBinding: RvItemRepoBinding) :
        ViewHolder(itemRepoBinding.root)
}