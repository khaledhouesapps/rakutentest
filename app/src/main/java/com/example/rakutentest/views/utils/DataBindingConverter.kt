package com.example.rakutentest.views.utils

object DataBindingConverter {

    fun toString(value: Long): String {
        return "$value"
    }

    fun toString(value: Boolean): String {
        return "$value"
    }
}