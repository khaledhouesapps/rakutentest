package com.example.rakutentest.views.repoDetails

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.browser.customtabs.CustomTabsIntent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.rakutentest.R
import com.example.rakutentest.models.entities.Repo
import com.example.rakutentest.databinding.ActivityRepoDetailsBinding
import com.example.rakutentest.models.entities.ui.RepoUi
import com.example.rakutentest.viewModels.RepoDetailsViewModel

class RepoDetailsActivity : AppCompatActivity() {
    private lateinit var activityBinding: ActivityRepoDetailsBinding
    private lateinit var viewModel: RepoDetailsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_details)

        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_repo_details)

        viewModel = ViewModelProvider(this).get(RepoDetailsViewModel::class.java)
        viewModel.repo = intent.getSerializableExtra("Repo") as RepoUi

        activityBinding.item = viewModel.repo

        initViewEvents()
        initView()
    }

    private fun initView() {
        Glide.with(activityBinding.root)
            .load(viewModel.repo?.ownerAvatar)
            .apply(RequestOptions.placeholderOf(R.drawable.ic_bitbucket))
            .into(activityBinding.imgOwnerAvatar)

        activityBinding.btnRepoUrl.isEnabled = viewModel.repo?.hasWebsite ?: false
    }

    private fun initViewEvents() {
        activityBinding.btnBack.setOnClickListener {
            finish()
        }

        activityBinding.btnRepoUrl.setOnClickListener {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent: CustomTabsIntent = builder.build()
            customTabsIntent.launchUrl(this, Uri.parse(viewModel.repo?.website))
        }
    }
}