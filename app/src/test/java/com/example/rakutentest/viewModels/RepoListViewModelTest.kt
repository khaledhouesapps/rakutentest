package com.example.rakutentest.viewModels

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.example.rakutentest.models.entities.api.callbacks.DataLoadingCallback
import com.example.rakutentest.models.entities.ui.RepoUi
import com.example.rakutentest.models.repositories.RepoRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RepoListViewModelTest {
    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: RepoListViewModel

    @Mock
    private lateinit var dataLoadingCallback: DataLoadingCallback

    @Mock
    private lateinit var repository: RepoRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel = RepoListViewModel(Application())
        viewModel.repoRepository = repository
    }

    @Test
    fun `when fetchRepositories called and reposLiveData null then repository fetchRepos should be called with wantNextPage false `() =
        runBlocking {
            viewModel.fetchRepositories(dataLoadingCallback)
            verify(repository).fetchRepos(false, dataLoadingCallback)
        }

    @Test
    fun `when fetchRepositories called and reposLiveData not null then repository fetchRepos should be called with wantNextPage true `() =
        runBlocking {
            viewModel.reposListLiveData = MutableLiveData(
                listOf(
                    RepoUi(
                        name = "",
                        language = "",
                        creationDate = "",
                        updateDate = "",
                        hasIssues = false,
                        forkPolicy = "",
                        hasWiki = false,
                        hasWebsite = false,
                        size = 0,
                        type = "",
                        website = "",
                        ownerName = "",
                        ownerType = "",
                        ownerAvatar = ""
                    )
                )
            )
            viewModel.fetchRepositories(dataLoadingCallback)
            verify(repository).fetchRepos(true, dataLoadingCallback)
        }
}