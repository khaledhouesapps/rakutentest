package com.example.rakutentest.models.repositories

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.rakutentest.models.entities.api.retrofitinstance.RetrofitClientInstance
import com.example.rakutentest.models.entities.api.services.IRepositoriesService
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.net.HttpURLConnection

@RunWith(MockitoJUnitRunner::class)
class RepoRepositoryTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    private lateinit var repository: RepoRepository
    private lateinit var mockWebServer: MockWebServer
    private lateinit var repoService: IRepositoriesService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repoService = RetrofitClientInstance.retrofitInstance?.create(
            IRepositoriesService::class.java
        )!!
        repository = RepoRepository(repoService)

        mockWebServer = MockWebServer()
        mockWebServer.start()
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `read sample success json file should not null`() {
        val reader = MockResponseFileReader("success_response.json")
        assertNotNull(reader.content)
    }

    @Test
    fun `fetch details and check response Code 200 returned should true`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("success_response.json").content)
        mockWebServer.enqueue(response)
        // Act
        val actualResponse = repository.reposService?.fetchRepositories()?.execute()
        // Assert
        assertEquals(
            response.toString().contains("200"),
            actualResponse?.code().toString().contains("200")
        )
    }

    @Test
    fun `fetch next and check same link returned should true`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("success_response.json").content)
        mockWebServer.enqueue(response)
        val mockResponse = response.getBody()?.readUtf8()
        // Act
        val actualResponse = repository.reposService?.fetchRepositories()?.execute()
        // Assert
        assertEquals(
            mockResponse?.let { `parse mocked JSON response next attribute`(it) },
            actualResponse?.body()?.nextPageLink
        )
    }

    @Test
    fun `fetch pageLen and check same number returned should true`() {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("success_response.json").content)
        mockWebServer.enqueue(response)
        val mockResponse = response.getBody()?.readUtf8()
        // Act
        val actualResponse = repository.reposService?.fetchRepositories()?.execute()
        // Assert
        assertEquals(
            mockResponse?.let { `parse mocked JSON response pagelen attribute`(it) },
            actualResponse?.body()?.itemsCount
        )
    }

    private fun `parse mocked JSON response next attribute`(mockResponse: String): String {
        val reader = JSONObject(mockResponse)
        return reader.getString("next")
    }

    private fun `parse mocked JSON response pagelen attribute`(mockResponse: String): Int {
        val reader = JSONObject(mockResponse)
        return reader.getInt("pagelen")
    }

}